package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    @FindBy(css = "*[type='email']")
    private WebElement emailInput;
    @FindBy(id = "identifierNext")
    private WebElement buttonMailNext;
    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordInput;
    @FindBy(xpath = "//*[@id=\"passwordNext\"]/span")
    private WebElement buttonPasswordNext;
    @FindBy(id = "profileIdentifier")
    private WebElement testFieldChecker;
    @FindBy(xpath = "//*[@id=\"gb\"]/div[2]/div[3]/div/div[2]/div/a")
    private WebElement userIcon;


    public LoginPage(WebDriver webDriver) {
        webDriver.get("https://mail.google.com");
        PageFactory.initElements(webDriver, this);
    }

    public WebElement getEmailInput() {
        return emailInput;
    }

    public WebElement getButtonMailNext() {
        return buttonMailNext;
    }

    public WebElement getPasswordInput() {
        return passwordInput;
    }

    public WebElement getButtonPasswordNext() {
        return buttonPasswordNext;
    }

    public WebElement getTestFieldChecker() {
        return testFieldChecker;
    }

    public WebElement getUserIcon() {
        return userIcon;
    }
}
