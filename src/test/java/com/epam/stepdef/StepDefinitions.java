package com.epam.stepdef;

import com.epam.actions.LogInActions;
import com.epam.driver.DriverInitializator;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class StepDefinitions {
  private LogInActions logInActions;

  @Given("user want to log in")
  public void userWantToLogIn() {
    logInActions = new LogInActions(DriverInitializator.getDriver("chrome"), 15);
  }

  @When("^user enter his/her (.*) and (.*)$")
  public void userEnterHisHerMailAndPassword(String mail, String password) {
    logInActions.logIn(mail, password);
  }

  @Then("^user sees his (.*)$")
  public void userSeesHisMail(String mail) {
    Assert.assertTrue(logInActions.assertLogIn(mail));
    DriverInitializator.quitDriver("chrome");
  }
}
